/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Singly Linked list
 *
 **********************************************************************/

/**
 * @addtogroup Singly Linked List
 * @{
 */

#ifndef _LIST_H_
#define _LIST_H_

#include <stdint.h>

/// Initializes a linked list element
#define LIST_ITEM_INIT(x)    {.id=(x), .next=NULL}

/// Linked list
struct list {
    uint32_t id;        ///< Identifier
    struct list* next;  ///< Next element
};

void list_insert(struct list **list, struct list *item, int32_t (*cmp)(struct list *a, struct list*b));
void list_delete(struct list** list, struct list* item);
void list_traverse(struct list* list, int32_t (*cb)(struct list *item));
void list_deinit(struct list **list);
void list_init(struct list **list);

#endif

/**
 * @}
 */
