

#include <stdio.h>
#include "list.h"
#include "unity.h"

#define ARRAY_SIZE(x)   (sizeof(x) / sizeof(x[0]))

static struct list* list = NULL;

struct list item1 = LIST_ITEM_INIT(1);
struct list item2 = LIST_ITEM_INIT(2);
struct list item3 = LIST_ITEM_INIT(3);
struct list item4 = LIST_ITEM_INIT(4);
struct list item5 = LIST_ITEM_INIT(5);

static int32_t list_indices[5];
static int32_t list_indices_cnt = 0;

static int32_t list_cb(struct list *item)
{
    list_indices[list_indices_cnt++] = item->id;

    return 0;
}

static int32_t list_cb1(struct list *item)
{
    list_indices[list_indices_cnt++] = item->id;
    if (list_indices_cnt > 1)
    {
        return 1;
    }

    return 0;
}

static int32_t cmp1(struct list *a, struct list *b)
{
    (void)a;
    (void)b;

    return 0;
}

static int32_t cmp2(struct list *a, struct list *b)
{
    (void)a;
    (void)b;

    return 1;
}

static int32_t cmp3(struct list *a, struct list *b)
{
    if (a->id > b->id)
    {
        return 1;
    }

    return 0;
}

static int32_t cmp4(struct list *a, struct list *b)
{
    if (a->id < b->id)
    {
        return 1;
    }

    return 0;
}

static void list_verify(int32_t *ref, int32_t ref_len)
{
    TEST_ASSERT_EQUAL_INT32(ref_len, list_indices_cnt);
    TEST_ASSERT_EQUAL_INT32_ARRAY(ref, list_indices, list_indices_cnt);
    list_indices_cnt = 0;
}

static void test_init(void)
{
    list_init(&list);
    TEST_ASSERT_NULL(list);
}

static void test_insert_tail(void)
{
    int32_t ref[] = {1, 2, 3, 4};

    list_deinit(&list);

    list_insert(&list, &item1, cmp1);
    list_insert(&list, &item2, cmp1);
    list_insert(&list, &item3, cmp1);
    list_insert(&list, &item4, cmp1);
    list_traverse(list, list_cb);

    list_verify(ref, ARRAY_SIZE(ref));
}

static void test_insert_head(void)
{
    int32_t ref[] = {4, 3, 2, 1};

    list_deinit(&list);

    list_insert(&list, &item1, cmp2);
    list_insert(&list, &item2, cmp2);
    list_insert(&list, &item3, cmp2);
    list_insert(&list, &item4, cmp2);
    list_traverse(list, list_cb);

    list_verify(ref, ARRAY_SIZE(ref));
}

static void test_insert_descend(void)
{
    int32_t ref[] = {4, 3, 2, 1};

    list_deinit(&list);

    list_insert(&list, &item4, cmp3);
    list_insert(&list, &item2, cmp3);
    list_insert(&list, &item3, cmp3);
    list_insert(&list, &item1, cmp3);
    list_traverse(list, list_cb);

    list_verify(ref, ARRAY_SIZE(ref));
}

static void test_insert_ascend(void)
{
    int32_t ref[] = {1, 2, 3, 4};

    list_deinit(&list);

    list_insert(&list, &item4, cmp4);
    list_insert(&list, &item2, cmp4);
    list_insert(&list, &item3, cmp4);
    list_insert(&list, &item1, cmp4);
    list_traverse(list, list_cb);

    list_verify(ref, ARRAY_SIZE(ref));
}

static void test_delete1(void)
{
    int32_t ref[] = {1, 2, 3, 4};

    list_delete(&list, &item5);

    list_traverse(list, list_cb);

    list_verify(ref, ARRAY_SIZE(ref));
}

static void test_delete2(void)
{
    int32_t ref[] = {1, 3, 4};

    list_delete(&list, &item2);
    TEST_ASSERT_NULL(item2.next);

    list_traverse(list, list_cb);

    list_verify(ref, ARRAY_SIZE(ref));
}

static void test_delete3(void)
{
    int32_t ref[] = {3, 4};

    list_delete(&list, &item1);
    TEST_ASSERT_NULL(item1.next);

    list_traverse(list, list_cb);

    list_verify(ref, ARRAY_SIZE(ref));
}

static void test_delete4(void)
{
    int32_t ref[] = {3};

    list_delete(&list, &item4);
    TEST_ASSERT_NULL(item4.next);

    list_traverse(list, list_cb);

    list_verify(ref, ARRAY_SIZE(ref));
}

static void test_delete5(void)
{
    list_delete(&list, &item3);
    TEST_ASSERT_NULL(item3.next);

    list_traverse(list, list_cb);

    TEST_ASSERT_NULL(list);
    TEST_ASSERT_EQUAL_INT32(0, list_indices_cnt);
}

static void test_traverse(void)
{
    int32_t ref[] = {1, 2};

    list_deinit(&list);

    list_insert(&list, &item1, cmp1);
    list_insert(&list, &item2, cmp1);
    list_insert(&list, &item3, cmp1);
    list_insert(&list, &item4, cmp1);
    list_traverse(list, list_cb1);

    list_verify(ref, ARRAY_SIZE(ref));
}

static void test_deinit(void)
{
    list_insert(&list, &item1, cmp1);
    list_insert(&list, &item2, cmp1);
    list_insert(&list, &item3, cmp1);

    list_deinit(&list);

    TEST_ASSERT_NULL(item1.next);
    TEST_ASSERT_NULL(item2.next);
    TEST_ASSERT_NULL(item3.next);
    TEST_ASSERT_NULL(list);
}

int main(int argc, char *argv[])
{
    UNITY_BEGIN();

    RUN_TEST(test_init);
    RUN_TEST(test_insert_tail);
    RUN_TEST(test_delete1);
    RUN_TEST(test_delete2);
    RUN_TEST(test_delete3);
    RUN_TEST(test_delete4);
    RUN_TEST(test_delete5);
    RUN_TEST(test_deinit);
    RUN_TEST(test_insert_head);
    RUN_TEST(test_insert_ascend);
    RUN_TEST(test_insert_descend);
    RUN_TEST(test_traverse);

    return UNITY_END();
}
