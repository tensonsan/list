/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Singly Linked list
 *
 **********************************************************************/

/**
 * @addtogroup Singly Linked List
 * @{
 */

#include <stdio.h>
#include <stdint.h>
#include "list.h"

/**
 * @brief Inserts an item into a singly linked list
 * 
 * @note The \p cmp function determines the type of insertion:
 * -# If the \p cmp always returns 0, items are inserted at the head
 * -# If the \p cmp always returns 1, items are appended at the tail
 * -# If the \p cmp returns 1 when a > b and 0 otherwise, descending item insertions
 * -# If the \p cmp returns 1 when a < b and 0 otherwise, ascending item insertions
 * 
 * @param[inout] list Pointer to pointer to the list
 * @param[in] item Item to be added to the list
 * @param[in] cmp Compare function
 */
void list_insert(struct list **list, struct list *item, int32_t (*cmp)(struct list *a, struct list*b))
{
    struct list *seek = *list;
    if ((NULL == seek) || cmp(item, seek))
    {
        item->next = seek;
        *list = item;
    }
    else
    {
        while (seek->next)
        {
            if (cmp(item, seek->next))
            {
                break;
            }
            seek = seek->next;
        }
        item->next = seek->next;
        seek->next = item;
    }
}

/**
 * @brief Removes an item from a singly linked list
 * 
 * @note An item is removed from an arbitrary location in the list
 *
 * @param[inout] list Pointer to pointer to the list
 * @param[in] item Item to be removed from the list
 */
void list_delete(struct list** list, struct list* item)
{
    if (*list)
    {
        if (*list == item)
        {
            *list = item->next;
            item->next = NULL;
        }
        else
        {
            struct list* seek = *list;
            while (seek->next)
            {
                if (seek->next == item)
                {
                    seek->next = seek->next->next;
                    item->next = NULL;
                    break;
                }
                seek = seek->next;
            }
        }
    }
}

/**
 * @brief Traverses the singly linked list
 * 
 * @note If \p cb returns 1 the traversal is stopped
 *
 * @param[in] list Pointer to the list
 * @param[in] cb Callback called per linked list item
 */
void list_traverse(struct list* list, int32_t (*cb)(struct list *item))
{
    struct list* seek = list;
    while (seek)
    {
        if (cb(seek))
        {
            break;
        }
        seek = seek->next;
    }
}

/**
 * Clears the singly linked list
 * 
 * @param[inout] list Pointer to pointer to the list
 */
void list_deinit(struct list **list)
{
    if (*list)
    {
        struct list *seek = *list;
        while (seek)
        {
            struct list *tmp = seek->next;
            seek->next = NULL;
            seek = tmp;
        }
        *list = NULL;
    }
}

/**
 * Initializes the singly linked list
 * 
 * @param[inout] list Pointer to pointer to the list
 */
void list_init(struct list **list)
{
    *list = NULL;
}

/**
 * @}
 */
