
TARGET = list_test
CC = gcc
LDFLAGS = -lm
BUILDDIR = bin

CFLAGS = -Wall -g -fprofile-arcs -ftest-coverage

SRC = $(wildcard *.c)
OBJ = $(addprefix $(BUILDDIR)/, $(addsuffix .o, $(basename $(SRC))))

$(TARGET): $(OBJ)
	$(CC) $(CFLAGS) -o $@ $(OBJ) $(LDFLAGS)

$(BUILDDIR)/%.o: %.c
	mkdir -p $(dir $@)
	$(CC) -c $(CFLAGS) $< -o $@

.PHONY: clean
clean:
	rm -rf $(BUILDDIR)
	rm -f $(TARGET)
	rm -rf doc
	rm -rf cov
	rm -rf coverage.info

coverage:
	lcov --capture --directory . --output-file coverage.info
	genhtml coverage.info --output-directory cov

run:
	./$(TARGET)

doc:
	doxygen Doxyfile
